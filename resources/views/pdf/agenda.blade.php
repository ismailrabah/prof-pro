<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Agenda</title>
</head>
<body>
    <style>
        body{
        }
        .head , .emploi{
            width: 100%;
            text-align: center
        }
        .head,.head td , .emploi, .emploi th , .emploi td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        .head_info{
            height: 30px;
        }
        .title{
            width: 50%;
            font-size: 2rem;
        }
        .emploi th {
            height: 50px;
            width: 35px;
            font-size: 14px;
            padding: 0;
            font-weight: 400 !important;
        }
        .rotate .container {
            position: relative;
            overflow: visible;
        }
        .rotate .content {
            overflow: visible;
            transform: rotate(-90deg);
            transform-origin: center center;
            position: relative;
            right: 0px;
            top:-30px;
        }
        .emploi tbody td{
            height: 40px;
        }
        
        .emploi tbody tr td:first-child{
            font-size: 11px;
            font-weight: 700
        }
        .logo img{
            width: 150px;
            height: 75px;
        }
    </style>

    <table class="head">
        <tr>
            <td class="logo" rowspan="3">
                <img src="vendor/jig/images/ofppt_logo.jpg" width="100" height="100" alt="OFPPT">
                <p><B>ISTA OULAD TEIMA</B></p>
            </td>
            <td class="title" rowspan="3">
                @if ($emploi_type == 'prof')
                    <b>EMPLIO DE FORMATEUR</b>
                @else
                    <b>EMPLIO DE GROUPE</b>
                @endif
            </td>
            <td class="head_info">
                DATE D'AFFECTATION 
            </td>
            <td class="head_info">
                <?php $today_date = Carbon\Carbon::now();
                echo $today_date->format('Y/m/d');?>
            </td>
        </tr>
        <tr>
            <td class="head_info" colspan="2"> </td>
        </tr>
        <tr>
            <td class="head_info" colspan="2"> </td>
        </tr>
    </table>
    <table style="margin-top: 20px;width:100%">
        <tr>
            <td style="width:60%">
                <strong>Année de Formation : {{$yearOfFormation}} </strong>    
            </td>
            <td>
                <strong>MASSE HORAIRE : {{ $mass_houres }} H/SEMAINE</strong>   
            </td>
        </tr>
    </table>
    
    <table style="margin-top: 10px;width:100%">
        @if ($emploi_type == 'prof')
            <tr>
                <td style="width:60%">
                    <strong>Formateur:  {{ $prof ? $prof->name : "" }} </strong>    
                </td>
            </tr>
        @else
            <tr>
                <td style="width:60%">
                    <strong>GROUPE:  {{ $group ? $group->name : "" }} </strong>    
                </td>
                <td>
                    <strong>NIVEAU :  {{ $group ? $group->level : "" }} </strong>   
                </td>
            </tr>
            <tr>
                <td style="width:60%">
                    <strong>MODE DE FORMATION:  {{$formation_mode}} </strong>    
                </td>
                <td>
                    <strong>MASSE HORAIRE/SEMAINE : {{ $mass_houres }} </strong>   
                </td>
            </tr>
        @endif
    </table>

    <table class="emploi" style="margin-top: 30px;margin-left:-15px">
        <thead>
            <tr>
                <th class="rotate" style="border-top:0px solide #fff;border-left:0px solide #fff"><div class="container"><div class="content" style="top:-45px !important">08h00</div></div></th>
                <th class="rotate"><div class="container"><div class="content"></div></div></th>
                <th class="rotate"><div class="container"><div class="content">09h00</div></div></th>
                <th class="rotate"><div class="container"><div class="content"></div></div></th>
                <th class="rotate"><div class="container"><div class="content">10h00</div></div></th>
                <th class="rotate"><div class="container"><div class="content"></div></div></th>
                <th class="rotate"><div class="container"><div class="content">11h00</div></div></th>
                <th class="rotate"><div class="container"><div class="content"></div></div></th>
                <th class="rotate"><div class="container"><div class="content">12h00</div></div></th>
                <th class="rotate"><div class="container"><div class="content"></div></div></th>
                <th class="rotate"><div class="container"><div class="content">13h00</div></div></th>
                <th class="rotate"><div class="container"><div class="content">13h30</div></div></th>
                <th class="rotate"><div class="container"><div class="content"></div></div></th>
                <th class="rotate"><div class="container"><div class="content">14h30</div></div></th>
                <th class="rotate"><div class="container"><div class="content"></div></div></th>
                <th class="rotate"><div class="container"><div class="content">15h30</div></div></th>
                <th class="rotate"><div class="container"><div class="content"></div></div></th>
                <th class="rotate"><div class="container"><div class="content">16h30</div></div></th>
                <th class="rotate"><div class="container"><div class="content"></div></div></th>
                <th class="rotate"><div class="container"><div class="content">17h30</div></div></th>
                <th class="rotate"><div class="container"><div class="content"></div></div></th>
                <th class="rotate"><div class="container"><div class="content">18h30</div></div></th>
            </tr> 
        </thead>
        <tbody>
            @foreach ($sessions as $day => $row)
                <?php $day_start_time = 0; ?>
                <?php $sesWith30min = 0; ?>
                <tr>
                    <td style="font-size: 10px;font-weight: 600">
                        {{ $day }}
                    </td>
                   @foreach ($row as $inx => $sesns)
                        @if ($sesns['s'] != 0)
                            @for ($i = $day_start_time - $sesWith30min; $i < ($sesns['s']); $i++)
                                @if($i != 10)
                                    <td style="background:#ccc"></td>
                                @else
                                    <td style="background:#fff;border-bottom:1px solide #fff"></td>
                                @endif
                            @endfor
                        @endif
                        <?php $day_start_time = $sesns['e'];?>
                        @if ($sesns['m'] >0)
                            <?php $day_start_time = $day_start_time+1;?>
                            <?php $sesWith30min = 1;?>                            
                        @else
                            <?php $sesWith30min = 0;?>
                        @endif
                        <td colspan="{{ ($sesns['h'] * 2) + $sesWith30min }}">
                            {{ $sesns['title'] }}
                        </td>
                   @endforeach
                   @if ($day_start_time < 21 )
                        @for ($i = $day_start_time - $sesWith30min; $i < 21; $i++)
                            @if($i != 10)
                                <td style="background:#ccc"></td>
                            @else
                                <td style="background:#fff;border-bottom:1px solide #fff"></td>
                            @endif
                        @endfor
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>