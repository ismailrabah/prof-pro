<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class SeedInitData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = DB::table('users')->where("email", "=", "admin@prof-pro.com")->first();

        if (Schema::hasTable('sales')) {
            DB::transaction(function () use ($user) {
                $sales = DB::table('sales')->count('*');
                if ($sales == 0) {
                    $sale1 = DB::table('sales')->insertGetId([ "name" => "Sale 1"]);
                    $sale2 = DB::table('sales')->insertGetId([ "name" => "Sale 2"]);
                    $sale3 = DB::table('sales')->insertGetId([ "name" => "Sale 3"]);
                }
            });
        } else {
            abort(500, "The sales table does not exist. Ensure you run the permissions migration before running this seeder.");
        }

        if (Schema::hasTable('session_type')) {
            DB::transaction(function () use ($user) {
                $session_type = DB::table('session_type')->count('*');
                if ($session_type == 0) {
                    $st1 = DB::table('session_type')->insertGetId([ "type" => "Théorique" , 'color' => 'bg-teal']);
                    $st2 = DB::table('session_type')->insertGetId([ "type" => "TP" , 'color' => 'bg-cyan'] );
                    $st3 = DB::table('session_type')->insertGetId([ "type" => "EFM" , 'color' => 'bg-orange']);
                }
            });
        } else {
            abort(500, "The session_type table does not exist. Ensure you run the permissions migration before running this seeder.");
        }
        
        if (Schema::hasTable('group')) {
            DB::transaction(function () use ($user) {
                $group = DB::table('group')->count('*');
                if ($group == 0) {
                    $st1 = DB::table('group')->insertGetId([ "name" => "TDI 1" ,'level'=> "1",  'total_members' => 22,"created_at" => now(),"updated_at" => now()]);
                    $st2 = DB::table('group')->insertGetId([ "name" => "TDI 2" ,'level'=> "2",  'total_members' => 21,"created_at" => now(),"updated_at" => now()]);
                    $st3 = DB::table('group')->insertGetId([ "name" => "TRI 1" ,'level'=> "1",  'total_members' => 30,"created_at" => now(),"updated_at" => now()]);
                    $st3 = DB::table('group')->insertGetId([ "name" => "TRI 2" ,'level'=> "2",  'total_members' => 33,"created_at" => now(),"updated_at" => now()]);
                }
            });
        } else {
            abort(500, "The group table does not exist. Ensure you run the permissions migration before running this seeder.");
        }
        
        if (Schema::hasTable('module')) {
            DB::transaction(function () use ($user) {
                $module = DB::table('module')->count('*');
                if ($module == 0) {
                    $st1 = DB::table('module')->insertGetId([ "name" => "POO 1" , 'level' => "1","start_date" => now(),"end_date" => now() , 'total_hours' => 100 , 'average' => 7]);
                    $st2 = DB::table('module')->insertGetId([ "name" => "POO 2" , 'level' => "2","start_date" => now(),"end_date" => now() , 'total_hours' => 120 , 'average' => 5]);
                    $st3 = DB::table('module')->insertGetId([ "name" => "SGBD" , 'level' => "1","start_date" => now(),"end_date" => now() , 'total_hours' => 90 , 'average' => 5]);
                    $st3 = DB::table('module')->insertGetId([ "name" => "UML" , 'level' => "2","start_date" => now(),"end_date" => now() , 'total_hours' => 60 , 'average' => 2.5]);
                }
            });
        } else {
            abort(500, "The module table does not exist. Ensure you run the permissions migration before running this seeder.");
        }
        
    }
}
