<?php

namespace App\Models;
/* Imports */
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    use HasFactory;
    protected $table = 'session';
    
    protected $fillable = [
        'start_date','end_date'
    ];
    
    
    
    protected $dates = [
        'start_date','end_date'
        ];
    public $timestamps = false;
    
    protected $appends = ["api_route", "can"];

    /* ************************ ACCESSOR ************************* */

    public function getApiRouteAttribute() {
        return route("api.sessions.index");
    }
    public function getCanAttribute() {
        return [
            "view" => \Auth::check() && \Auth::user()->can("view", $this),
            "update" => \Auth::check() && \Auth::user()->can("update", $this),
            "delete" => \Auth::check() && \Auth::user()->can("delete", $this),
            "restore" => \Auth::check() && \Auth::user()->can("restore", $this),
            "forceDelete" => \Auth::check() && \Auth::user()->can("forceDelete", $this),
        ];
    }

    protected function serializeDate(DateTimeInterface $date) {
        return $date->format('Y-m-d H:i:s');
    }

    /* ************************ RELATIONS ************************ */
    public function user(){
        return $this->belongsTo(User::class , 'user_id');
    }
    public function group(){
        return $this->belongsTo(Group::class , 'group_id');
    }
    public function module(){
        return $this->belongsTo(Module::class , 'module_id');
    }
    public function sessionType(){
        return $this->belongsTo(SessionType::class , 'session_type_id');
    }
    public function sale(){
        return $this->belongsTo(Sale::class , 'sale_id');
    }
}
