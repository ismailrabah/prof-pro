<?php

namespace App\Models;
/* Imports */
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Absent extends Model
{
    use HasFactory;
protected $table = 'absent';
    
    protected $fillable = [
        'reason',
        'session_id',
        'student_id',
    ];
    
    
    
    protected $dates = [
            'created_at','updated_at',
    ];

    protected $appends = ["api_route", "can"];

    /* ************************ ACCESSOR ************************* */

    public function getApiRouteAttribute() {
        return route("api.absents.index");
    }
    public function getCanAttribute() {
        return [
            "view" => \Auth::check() && \Auth::user()->can("view", $this),
            "update" => \Auth::check() && \Auth::user()->can("update", $this),
            "delete" => \Auth::check() && \Auth::user()->can("delete", $this),
            "restore" => \Auth::check() && \Auth::user()->can("restore", $this),
            "forceDelete" => \Auth::check() && \Auth::user()->can("forceDelete", $this),
        ];
    }

    protected function serializeDate(DateTimeInterface $date) {
        return $date->format('Y-m-d H:i:s');
    }

    /* ************************ RELATIONS ************************ */
    /**
    * Many to One Relationship to \App\Models\Session::class
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function session() {
        return $this->belongsTo(\App\Models\Session::class,"session_id","id");
    }
    /**
    * Many to One Relationship to \App\Models\Student::class
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function student() {
        return $this->belongsTo(\App\Models\Student::class,"student_id","id");
    }
}
