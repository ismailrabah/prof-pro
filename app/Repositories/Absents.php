<?php
namespace App\Repositories;

use App\Models\Absent;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Column;

class Absents
{
    private Absent $model;
    public static function init(Absent $model): Absents
    {
        $instance = new self;
        $instance->model = $model;
        return $instance;
    }

    public static function store(object $data): Absent
    {
        $model = new Absent((array) $data);
                // Save Relationships
            

        if (isset($data->session)) {
            $model->session()
                ->associate($data->session->id);
        }
        if (isset($data->student)) {
            $model->student()
                ->associate($data->student->id);
        }
                    

        $model->saveOrFail();
        return $model;
    }

    public function show(Request $request): Absent {
        //Fetch relationships
                $this->model->load([
            'session',
            'student',
        ]);
    return $this->model;
    }
    public function update(object $data): Absent
    {
        $this->model->update((array) $data);
        
        // Save Relationships
                

        if (isset($data->session)) {
            $this->model->session()
                ->associate($data->session->id);
        }


        if (isset($data->student)) {
            $this->model->student()
                ->associate($data->student->id);
        }
                

        $this->model->saveOrFail();
        return $this->model;
    }

    public function destroy(): bool
    {
        return !!$this->model->delete();
    }
    public static function dtColumns() {
        $columns = [
            Column::make('id')->title('ID')->className('all text-right'),
            Column::make("student")->className('min-desktop-lg'),
            Column::make("module")->className('min-desktop-lg'),
            Column::make("session_date")->className('min-desktop-lg'),
            Column::make("reason")->className('min-desktop-lg'),
            Column::make('actions')->className('min-desktop text-right')->orderable(false)->searchable(false),
        ];
        return $columns;
    }
    public static function dt($query) {
        return DataTables::of($query)
            ->addColumn('student', function (Absent $absent) {
                return $absent->student->name ?? "";
            })
            ->addColumn('module', function (Absent $absent) {
                return $absent->session->module->name ?? "";
            })
            ->addColumn('session_date', function (Absent $absent) {
                return $absent->session->start_date ?? "";
            })
            ->editColumn('actions', function (Absent $model) {
                $actions = '';
                if (\Auth::user()->can('view',$model)) $actions .= '<button class="bg-primary hover:bg-primary-600 p-2 px-3 focus:ring-0 focus:outline-none text-white action-button" title="View Details" data-action="show-model" data-tag="button" data-id="'.$model->id.'"><i class="fas fa-eye"></i></button>';
                if (\Auth::user()->can('update',$model)) $actions .= '<button class="bg-secondary hover:bg-secondary-600 p-2 px-3 focus:ring-0 focus:outline-none action-button" title="Edit Record" data-action="edit-model" data-tag="button" data-id="'.$model->id.'"><i class="fas fa-edit"></i></button>';
                if (\Auth::user()->can('delete',$model)) $actions .= '<button class="bg-danger hover:bg-danger-600 p-2 px-3 text-white focus:ring-0 focus:outline-none action-button" title="Delete Record" data-action="delete-model" data-tag="button" data-id="'.$model->id.'"><i class="fas fa-trash"></i></button>';
                return "<div class='gap-x-1 flex w-full justify-end'>".$actions."</div>";
            })
            ->rawColumns(['actions'])
            ->make();
    }
}
