<?php
namespace App\Repositories;

use App\Models\Session;
use App\Models\User;
use App\Models\Group;
use Carbon\Carbon;
use DateTime;
use Exception;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Column;

class Sessions
{
    private Session $model;

    public static function init(Session $model): Sessions{
        $instance = new self;
        $instance->model = $model;
        return $instance;
    }

    public static function store(object $data): Session{
        $model = new Session((array) $data);
        // Save Relationships "2022-05-06 13:00:0"
        $start_date = Carbon::createFromFormat('Y-m-d H:i:s',  $data->start_date."0");
        $end_date = Carbon::createFromFormat('Y-m-d H:i:s',  $data->end_date."0");
        if (isset($data->group)) {
            $sessions = Session::where(function($query) use ($data , $start_date , $end_date){
                $query->where('start_date' , '<' , $end_date)
                    ->where('end_date' , '>' , $start_date);
                $query->where('group_id' , '=' , $data->group->id);
            })->count();
            if($sessions > 0){
                throw new \Exception('This Group is not available at the selected time ! Please select different start/end time');
            }
            $model->group()->associate($data->group->id);
        }
        if (isset($data->module)) {
            $model->module()->associate($data->module->id);
        }
        if (isset($data->session_type)) {
            $model->sessionType()->associate($data->session_type->id);
        }
        if (isset($data->sale)) {
            $sessions = Session::where(function($query) use ($data , $start_date , $end_date){
                $query->where('start_date' , '<' , $end_date)
                    ->where('end_date' , '>' , $start_date);
                $query->where('sale_id' , '=' , $data->sale->id);
            })->count();
            if($sessions > 0){
                throw new \Exception('This Sale is not available at the selected time ! Please select different start/end time');
            }
            $model->sale()->associate($data->sale->id);
        }
        if (isset($data->user)) {
            $sessions = Session::where(function($query) use ($data , $start_date , $end_date){
                $query->where('start_date' , '<' , $end_date)
                    ->where('end_date' , '>' , $start_date);
                $query->where('user_id' , '=' , $data->user->id);
            })->count();
            if($sessions > 0){
                throw new \Exception('This Professor is not available at the selected time ! Please select different start/end time');
            }
            $model->user()->associate($data->user->id);
        }
        $model->saveOrFail();
        return $model;
    }

    public function show(Request $request): Session {
        //Fetch relationships
        $this->model->load([
            'group',
            'module',
            'sessionType',
            'user',
            'sale'
        ]);
        return $this->model;
    }

    public function update(object $data): Session{
        $sess_id = $this->model->id;
        // Save Relationships
        $time_changed = false;
        if($data->start_date != $this->model->start_date ){
            $time_changed = true; 
            $start_date = Carbon::createFromFormat('Y-m-d H:i:s',  $data->start_date."0");
        }else{
            $start_date = $this->model->start_date ;
        }
        if($data->end_date != $this->model->end_date){
            $time_changed = true;
            $end_date = Carbon::createFromFormat('Y-m-d H:i:s',  $data->end_date."0");
        }else{
            $end_date = $this->model->end_date;
        }
        if (isset($data->group)) {
            if($time_changed || $data->group->id != $this->model->group->id){
                $sessions = Session::where(function($query) use ($sess_id,$data , $start_date , $end_date ){
                    $query->where('start_date' , '<' , $end_date)
                        ->where('end_date' , '>' , $start_date);
                    $query->where('group_id' , '=' , $data->group->id);
                    $query->where('id' , '<>' , $sess_id);
                })->count();
                if($sessions > 0){
                    throw new \Exception('This Group is not available at the selected time ! Please select different start/end time');
                }
            }
            $this->model->group()->associate($data->group->id);
        }
        if (isset($data->module)) {
            $this->model->module()->associate($data->module->id);
        }
        if (isset($data->session_type)) {
            $this->model->sessionType()->associate($data->session_type->id);
        }
        if (isset($data->sale)) {
            if($time_changed || $data->sale->id != $this->model->sale->id ){
                $sessions = Session::where(function($query) use ($sess_id,$data , $start_date , $end_date ){
                    $query->where('start_date' , '<' , $end_date)
                        ->where('end_date' , '>' , $start_date);
                    $query->where('sale_id' , '=' , $data->sale->id);
                    $query->where('id' , '<>' , $sess_id);
                })->count();
                if($sessions > 0){
                    throw new \Exception('This Sale is not available at the selected time ! Please select different start/end time');
                }
            }
            $this->model->sale()->associate($data->sale->id);
        }
        if (isset($data->user)) {
            if($time_changed || $data->user->id != $this->model->user->id ){
                $sessions = Session::where(function($query) use ($sess_id,$data , $start_date , $end_date ){
                    $query->where('start_date' , '<' , $end_date)
                        ->where('end_date' , '>' , $start_date);
                    $query->where('user_id' , '=' , $data->user->id);
                    $query->where('id' , '<>' , $sess_id);
                })->count();
                if($sessions > 0){
                    throw new \Exception('This Professor is not available at the selected time ! Please select different start/end time');
                }
            }
            $this->model->user()->associate($data->user->id);
        }
        $this->model->update((array) $data);
        $this->model->saveOrFail();
        return $this->model;
    }

    public function destroy(): bool{
        return !!$this->model->delete();
    }

    public static function dtColumns() {
        $columns = [
            Column::make('id')->title('ID')->className('all text-right'),
            Column::make("module")->className('min-desktop-lg'),
            Column::make("group")->className('min-desktop-lg'),
            Column::make("session_type")->className('min-desktop-lg'),
            Column::make("sale")->className('min-desktop-lg'),
            Column::make("prof")->className('min-desktop-lg'),
            Column::make("start_date")->className('min-desktop-lg'),
            Column::make("end_date")->className('min-desktop-lg'),
            Column::make('actions')->className('min-desktop text-right')->orderable(false)->searchable(false),
        ];
        return $columns;
    }

    public static function dt($query) {
        return DataTables::of($query)
            ->addColumn('module', function (Session $session) {
                return $session->module->name;
            })
            ->addColumn('group', function (Session $session) {
                return $session->group->name;
            })
            ->addColumn('session_type', function (Session $session) {
                return $session->sessionType ? $session->sessionType->type : '';
            })
            ->addColumn('sale', function (Session $session) {
                return $session->sale ? $session->sale->name : '';
            })
            ->addColumn('prof', function (Session $session) {
                return $session->user->name;
            })
            ->editColumn('actions', function (Session $model) {
                $actions = '';
                if (\Auth::user()->can('view',$model)) $actions .= '<button class="bg-primary hover:bg-primary-600 p-2 px-3 focus:ring-0 focus:outline-none text-white action-button" title="View Details" data-action="show-model" data-tag="button" data-id="'.$model->id.'"><i class="fas fa-eye"></i></button>';
                if (\Auth::user()->can('update',$model)) $actions .= '<button class="bg-secondary hover:bg-secondary-600 p-2 px-3 focus:ring-0 focus:outline-none action-button" title="Edit Record" data-action="edit-model" data-tag="button" data-id="'.$model->id.'"><i class="fas fa-edit"></i></button>';
                if (\Auth::user()->can('delete',$model)) $actions .= '<button class="bg-danger hover:bg-danger-600 p-2 px-3 text-white focus:ring-0 focus:outline-none action-button" title="Delete Record" data-action="delete-model" data-tag="button" data-id="'.$model->id.'"><i class="fas fa-trash"></i></button>';
                return "<div class='gap-x-1 flex w-full justify-end'>".$actions."</div>";
            })
            ->rawColumns(['actions'])
            ->make();
    }

    public static function agenda_event($start_date, $end_date, $user = null, $group = null, $module = null){
        $items = [];
        $sessions = Session::where(function($query) use ($start_date, $end_date, $user, $group, $module){
            $query->where('start_date' , '<' , $end_date)
                ->where('end_date' , '>=' , $start_date);
                if($user)
                    $query->where('user_id' , '=' , $user);
                if($group)
                    $query->where('group_id' , '=' , $group);
                if($module)
                    $query->where('module_id' , '=' , $module);
            
        })->get();
        foreach ($sessions as $sess) {
            $color = !$sess->sessionType ? 'bg-teal' : (!$sess->sessionType->color ? 'bg-teal' : $sess->sessionType->color);
            $items[] = [
                'id' => 's_'.$sess->id,
                'startDate' => $sess->start_date,
                'endDate' =>  $sess->end_date,
                'title' => $sess->sessionType ? $sess->module->name ." ". $sess->sessionType->type. " " . $sess->group->name : $sess->module->name ." ". $sess->group->name,
                'classes' => [$color],
                "module"=> $sess->module->name,
                "sessionType" => $sess->sessionType ? $sess->sessionType->type : '',
                "sale" => $sess->sale ? $sess->sale->name : '',
                "group"=> $sess->group->name,
                "user"=> $sess->user->name
                // 'url' => "",
                // 'style' => "",
            ];
        }
        return $items;   
    }

    public static function handlerAgendaPeriod($displayPeriodUom , $displayPeriodCount , $start_date){
        $end_date = Carbon::createFromFormat('m/d/Y',  $start_date);
        if(!$displayPeriodCount)
            $displayPeriodCount = 1;
        switch ($displayPeriodUom){
            case "month":
                $end_date->addMonths($displayPeriodCount);
                $end_date->addWeeks(3);
                break;
            case "week":
                $end_date->addWeeks($displayPeriodCount);
                $end_date->addWeeks(1);
              break;
            case "year":
                $end_date->addYears($displayPeriodCount);
              break;
            default:
              $end_date->addMonths(1);
        }
        return $end_date;
    }

    /**
     * Get one week agenda sessions by user , group and module
     */
    public static function week_wessions($param){
        if(array_key_exists('start_date' , $param ) && array_key_exists('cv_week_number' , $param) ){
            $start_date = Carbon::createFromFormat('m/d/Y',  $param['start_date']);
            $startAndEndDate = self::getStartAndEndDate( intval($param['cv_week_number']) -1, $start_date->year);
            $user_id = null;
            $group_id = null;
            $module_id = null;
            $emploi_type = 'groupe';
            $group = null;
            $prof = null;
            
            if(array_key_exists('user_id' , $param )){
                $user_id = $param['user_id'];
                $emploi_type = 'prof';
                $prof = User::where('id' ,'=' ,$user_id)->first();
            }

            if(array_key_exists('group_id' , $param )){
                $group_id = $param['group_id'];
                $emploi_type = 'groupe';
                $group = Group::where('id' ,'=' ,$group_id)->first();
            }

            if(array_key_exists('module_id' , $param )){
                $module_id = $param['module_id'];
            }

            $sessions = Session::where(function($query) use ($startAndEndDate, $user_id, $group_id, $module_id){
                $query->where('start_date' , '<' , $startAndEndDate['week_end'])
                    ->where('end_date' , '>=' , $startAndEndDate['week_start']);
                    if($user_id)
                        $query->where('user_id' , '=' , $user_id);
                    if($group_id)
                        $query->where('group_id' , '=' , $group_id);
                    if($module_id)
                        $query->where('module_id' , '=' , $module_id);
            })->get();

            $sessions2week = self::session2WeeklyFormat($sessions , $startAndEndDate['week_start'] , $startAndEndDate['week_end']);

            return array_merge( ["emploi_type" => $emploi_type , 'group' => $group , 'prof' => $prof], $sessions2week );
        }
        return [];
    }

    /**
     * Get Start/End week date by week number and year
     */
    public static function getStartAndEndDate($week, $year) {
        $date = Carbon::now(); 
        $date->setISODate($year , $week); 
        $date2 = clone $date;
        $ret['week_start'] = $date->startOfWeek(); 
        $ret['week_end'] = $date2->endOfWeek(); 
        return $ret;
    }

    /**
     * Forma agenda sessions to emploi table based on week days(monday to sanday) and day hours (08:00 to 18:30) 
     */
    public static function session2WeeklyFormat($sessions , $week_start , $week_end){
        $sessions2week = ['LUNDI' => [],'MARDI' => [],'MERCREDI' => [],'JEUDI' => [],'VENDREDI' => [],'SAMEDI' => []];
        $days = ['LUNDI','MARDI','MERCREDI','JEUDI','VENDREDI','SAMEDI'];
        $hours = ['08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30'];
        $total_houres = 0;
        $total_mini = 0;
        foreach( $sessions as $sess ){
            $days_diff = $week_start->diff($sess->start_date)->days;
            if($days_diff<6){
                $lenght = $sess->start_date->diff($sess->end_date);
                $total_houres += $lenght->h;
                $total_mini += $lenght->i;
                $sessions2week[$days[$days_diff]][] = [ 
                    'start_date' =>$sess->start_date->format('H:i'),
                    'end_date' =>$sess->end_date->format('H:i'),
                    's' => array_search($sess->start_date->format('H:i'), $hours) ,
                    'e' => array_search($sess->end_date->format('H:i') , $hours),
                    'h' => $lenght->h,
                    'm' => $lenght->i,
                    'title' => $sess->sessionType ? $sess->module->name ." ". $sess->sessionType->type. " " . $sess->sale->name : $sess->module->name ." ". $sess->sale->name
                ];
            }
        }
        $res = $total_mini % 60;
        $total_mini = $total_mini - $res;
        $total_houres += $total_mini / 60;
        $mass_houres = $total_houres .":".$res;
        foreach($sessions2week as &$ds ){
            usort($ds , function($a,$b){ return $a['s'] > $b['s']; });
        }
        return ["sessions" => $sessions2week , 'mass_houres' => $mass_houres];
    }

    /**
     * Get Current formation year depanding on current month (from 09 of curren year to 06 of next year)
     */
    public static function yearOfFormation():string{
        $date = Carbon::now(); 
        $year = $date->year;
        if ($date->month > 6 )
            return $year ."/". $year + 1;
        else
            return $year - 1 ."/". $year;
    }
}
