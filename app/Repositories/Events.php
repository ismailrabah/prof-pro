<?php
namespace App\Repositories;

use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Column;

class Events
{
    private Event $model;
    public static function init(Event $model): Events
    {
        $instance = new self;
        $instance->model = $model;
        return $instance;
    }

    public static function store(object $data): Event
    {
        $model = new Event((array) $data);
                // Save Relationships
                    

        $model->saveOrFail();
        return $model;
    }

    public function show(Request $request): Event {
        //Fetch relationships
            return $this->model;
    }
    public function update(object $data): Event
    {
        $this->model->update((array) $data);
        
        // Save Relationships
                        

        $this->model->saveOrFail();
        return $this->model;
    }

    public function destroy(): bool
    {
        return !!$this->model->delete();
    }
    public static function dtColumns() {
        $columns = [
            Column::make('id')->title('ID')->className('all text-right'),
            Column::make("name")->className('all'),
            Column::make("type")->className('min-desktop-lg'),
            Column::make("color")->className('min-desktop-lg'),
            Column::make("start_date")->className('min-desktop-lg'),
            Column::make("end_date")->className('min-desktop-lg'),
            Column::make('actions')->className('min-desktop text-right')->orderable(false)->searchable(false),
        ];
        return $columns;
    }
    public static function dt($query) {
        return DataTables::of($query)
            ->editColumn('actions', function (Event $model) {
                $actions = '';
                if (\Auth::user()->can('view',$model)) $actions .= '<button class="bg-primary hover:bg-primary-600 p-2 px-3 focus:ring-0 focus:outline-none text-white action-button" title="View Details" data-action="show-model" data-tag="button" data-id="'.$model->id.'"><i class="fas fa-eye"></i></button>';
                if (\Auth::user()->can('update',$model)) $actions .= '<button class="bg-secondary hover:bg-secondary-600 p-2 px-3 focus:ring-0 focus:outline-none action-button" title="Edit Record" data-action="edit-model" data-tag="button" data-id="'.$model->id.'"><i class="fas fa-edit"></i></button>';
                if (\Auth::user()->can('delete',$model)) $actions .= '<button class="bg-danger hover:bg-danger-600 p-2 px-3 text-white focus:ring-0 focus:outline-none action-button" title="Delete Record" data-action="delete-model" data-tag="button" data-id="'.$model->id.'"><i class="fas fa-trash"></i></button>';
                return "<div class='gap-x-1 flex w-full justify-end'>".$actions."</div>";
            })
            ->rawColumns(['actions'])
            ->make();
    }
    
    public static function agenda_event($start_date , $end_date){
        $items = [];
        $vents = Event::where(function($query) use ($start_date , $end_date){
            $query->where('start_date' , '<' , $end_date)
                    ->where('end_date' , '>=' , $start_date);
        })->get();

        foreach ($vents as $event) {
            $color = !$event->color ? 'bg-yellow' : $event->color;
            $items[] = [
                'id' => 'e_'.$event->id,
                'startDate' => $event->start_date,
                'endDate' =>  $event->end_date,
                'title' => $event->name ." ". $event->type,
                'classes' => [ $color ],
                'name'=> $event->name,
                'type'=> $event->type,
                // 'url' => "",
                // 'style' => "",
            ];
        }
        return $items;   
    }
}
