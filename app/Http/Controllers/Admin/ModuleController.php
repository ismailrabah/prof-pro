<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\Module\IndexModule;
use App\Http\Requests\Module\StoreModule;
use App\Http\Requests\Module\UpdateModule;
use App\Http\Requests\Module\DestroyModule;
use App\Models\Module;
use App\Repositories\Modules;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Inertia\Inertia;
use Yajra\DataTables\Html\Column;

class ModuleController  extends Controller
{
    private Modules $repo;
    public function __construct(Modules $repo)
    {
        $this->repo = $repo;
    }

    /**
    * Display a listing of the resource.
    *
    * @param  Request $request
    * @return    \Inertia\Response
    * @throws  \Illuminate\Auth\Access\AuthorizationException
    */
    public function index(Request $request): \Inertia\Response
    {
        $this->authorize('viewAny', Module::class);
        return Inertia::render('Modules/Index',[
            "can" => [
                "viewAny" => \Auth::user()->can('viewAny', Module::class),
                "create" => \Auth::user()->can('create', Module::class),
            ],
            "columns" => $this->repo::dtColumns(),
        ]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return  \Inertia\Response
    */
    public function create()
    {
        $this->authorize('create', Module::class);
        return Inertia::render("Modules/Create",[
            "can" => [
            "viewAny" => \Auth::user()->can('viewAny', Module::class),
            "create" => \Auth::user()->can('create', Module::class),
            ]
        ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param StoreModule $request
    * @return \Illuminate\Http\RedirectResponse
    */
    public function store(StoreModule $request)
    {
        try {
            $data = $request->sanitizedObject();
            $module = $this->repo::store($data);
            return back()->with(['success' => "The Module was created succesfully."]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Display the specified resource.
    *
    * @param Request $request
    * @param Module $module
    * @return \Inertia\Response|\Illuminate\Http\RedirectResponse
    */
    public function show(Request $request, Module $module)
    {
        try {
            $this->authorize('view', $module);
            $model = $this->repo::init($module)->show($request);
            return Inertia::render("Modules/Show", ["model" => $model]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Show Edit Form for the specified resource.
    *
    * @param Request $request
    * @param Module $module
    * @return \Inertia\Response|\Illuminate\Http\RedirectResponse
    */
    public function edit(Request $request, Module $module)
    {
        try {
            $this->authorize('update', $module);
            //Fetch relationships
            

                        return Inertia::render("Modules/Edit", ["model" => $module]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Update the specified resource in storage.
    *
    * @param UpdateModule $request
    * @param {$modelBaseName} $module
    * @return \Illuminate\Http\RedirectResponse
    */
    public function update(UpdateModule $request, Module $module)
    {
        try {
            $data = $request->sanitizedObject();
            $res = $this->repo::init($module)->update($data);
            return back()->with(['success' => "The Module was updated succesfully."]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param Module $module
    * @return \Illuminate\Http\RedirectResponse
    */
    public function destroy(DestroyModule $request, Module $module)
    {
        $res = $this->repo::init($module)->destroy();
        if ($res) {
            return back()->with(['success' => "The Module was deleted succesfully."]);
        }
        else {
            return back()->with(['error' => "The Module could not be deleted."]);
        }
    }
}
