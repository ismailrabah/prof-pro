<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Absent;
use App\Models\Event;
use App\Models\Session;
use App\Repositories\Absents;
use App\Repositories\Events;
use App\Repositories\Sessions;
use Illuminate\Http\Request;
use Inertia\Inertia;

class AgendaController  extends Controller
{

    
    private Sessions $session_repo;
    private Events $event_repo;
    private Absents $absent_repo;

    public function __construct(Sessions $s_repo , Events $e_repo , Absents $a_repo)
    {
        $this->session_repo = $s_repo;
        $this->event_repo = $e_repo;
        $this->absent_repo = $a_repo;
    }

    /**
    * Display a listing of the resource.
    *
    * @param  Request $request
    * @return    \Inertia\Response
    * @throws  \Illuminate\Auth\Access\AuthorizationException
    */
    public function index(Request $request): \Inertia\Response
    {
        // $this->authorize('viewAny', Agenda::class);
        return Inertia::render('Agendas/Index',[
            "can" => [
                // "viewAny_session" => \Auth::user()->can('viewAny', Session::class),
                // "create_session" => \Auth::user()->can('create',Session ::class),
                // "update_session" => \Auth::user()->can('update',Session ::class),
                // "delete_session" => \Auth::user()->can('delete',Session ::class),
                
                // "viewAny_event" => \Auth::user()->can('viewAny', Event::class),
                // "create_event" => \Auth::user()->can('create',Event ::class),
                // "update_event" => \Auth::user()->can('update',Event ::class),
                // "delete_event" => \Auth::user()->can('delete',Event ::class),
                
                // "viewAny_absent" => \Auth::user()->can('viewAny', Absent::class),
                // "create_absent" => \Auth::user()->can('create',Absent ::class),
                // "update_absent" => \Auth::user()->can('update',Absent ::class),
                // "delete_absent" => \Auth::user()->can('delete',Absent ::class),
            ],

            // "columns" => $this->repo::dtColumns(),
            "agenda_items" => []
        ]);
    }

}
