<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\Session\IndexSession;
use App\Http\Requests\Session\StoreSession;
use App\Http\Requests\Session\UpdateSession;
use App\Http\Requests\Session\DestroySession;
use App\Models\Session;
use App\Repositories\Sessions;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Inertia\Inertia;
use Yajra\DataTables\Html\Column;

class SessionController  extends Controller
{
    private Sessions $repo;
    public function __construct(Sessions $repo)
    {
        $this->repo = $repo;
    }

    /**
    * Display a listing of the resource.
    *
    * @param  Request $request
    * @return    \Inertia\Response
    * @throws  \Illuminate\Auth\Access\AuthorizationException
    */
    public function index(Request $request): \Inertia\Response
    {
        $this->authorize('viewAny', Session::class);
        return Inertia::render('Sessions/Index',[
            "can" => [
                "viewAny" => \Auth::user()->can('viewAny', Session::class),
                "create" => \Auth::user()->can('create', Session::class),
            ],
            "columns" => $this->repo::dtColumns(),
        ]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return  \Inertia\Response
    */
    public function create()
    {
        $this->authorize('create', Session::class);
        return Inertia::render("Sessions/Create",[
            "can" => [
            "viewAny" => \Auth::user()->can('viewAny', Session::class),
            "create" => \Auth::user()->can('create', Session::class),
            ]
        ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param StoreSession $request
    * @return \Illuminate\Http\RedirectResponse
    */
    public function store(StoreSession $request)
    {
        try {
            $data = $request->sanitizedObject();
            $session = $this->repo::store($data);
            return back()->with(['success' => "The Session was created succesfully."]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Display the specified resource.
    *
    * @param Request $request
    * @param Session $session
    * @return \Inertia\Response|\Illuminate\Http\RedirectResponse
    */
    public function show(Request $request, Session $session)
    {
        try {
            $this->authorize('view', $session);
            $model = $this->repo::init($session)->show($request);
            return Inertia::render("Sessions/Show", ["model" => $model]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Show Edit Form for the specified resource.
    *
    * @param Request $request
    * @param Session $session
    * @return \Inertia\Response|\Illuminate\Http\RedirectResponse
    */
    public function edit(Request $request, Session $session)
    {
        try {
            $this->authorize('update', $session);
            //Fetch relationships
            
        $session->load([
            'group',
            'module',
            'sessionType',
            'user',
            'sale',
        ]);
                        return Inertia::render("Sessions/Edit", ["model" => $session]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Update the specified resource in storage.
    *
    * @param UpdateSession $request
    * @param {$modelBaseName} $session
    * @return \Illuminate\Http\RedirectResponse
    */
    public function update(UpdateSession $request, Session $session)
    {
        try {
            $data = $request->sanitizedObject();
            $res = $this->repo::init($session)->update($data);
            return back()->with(['success' => "The Session was updated succesfully."]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param Session $session
    * @return \Illuminate\Http\RedirectResponse
    */
    public function destroy(DestroySession $request, Session $session)
    {
        $res = $this->repo::init($session)->destroy();
        if ($res) {
            return back()->with(['success' => "The Session was deleted succesfully."]);
        }
        else {
            return back()->with(['error' => "The Session could not be deleted."]);
        }
    }
}
