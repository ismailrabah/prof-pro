<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\Absent\IndexAbsent;
use App\Http\Requests\Absent\StoreAbsent;
use App\Http\Requests\Absent\UpdateAbsent;
use App\Http\Requests\Absent\DestroyAbsent;
use App\Models\Absent;
use App\Repositories\Absents;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Inertia\Inertia;
use Yajra\DataTables\Html\Column;

class AbsentController  extends Controller
{
    private Absents $repo;
    public function __construct(Absents $repo)
    {
        $this->repo = $repo;
    }

    /**
    * Display a listing of the resource.
    *
    * @param  Request $request
    * @return    \Inertia\Response
    * @throws  \Illuminate\Auth\Access\AuthorizationException
    */
    public function index(Request $request): \Inertia\Response
    {
        $this->authorize('viewAny', Absent::class);
        return Inertia::render('Absents/Index',[
            "can" => [
                "viewAny" => \Auth::user()->can('viewAny', Absent::class),
                "create" => \Auth::user()->can('create', Absent::class),
            ],
            "columns" => $this->repo::dtColumns(),
        ]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return  \Inertia\Response
    */
    public function create()
    {
        $this->authorize('create', Absent::class);
        return Inertia::render("Absents/Create",[
            "can" => [
            "viewAny" => \Auth::user()->can('viewAny', Absent::class),
            "create" => \Auth::user()->can('create', Absent::class),
            ]
        ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param StoreAbsent $request
    * @return \Illuminate\Http\RedirectResponse
    */
    public function store(StoreAbsent $request)
    {
        try {
            $data = $request->sanitizedObject();
            $absent = $this->repo::store($data);
            return back()->with(['success' => "The Absent was created succesfully."]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Display the specified resource.
    *
    * @param Request $request
    * @param Absent $absent
    * @return \Inertia\Response|\Illuminate\Http\RedirectResponse
    */
    public function show(Request $request, Absent $absent)
    {
        try {
            $this->authorize('view', $absent);
            $model = $this->repo::init($absent)->show($request);
            return Inertia::render("Absents/Show", ["model" => $model]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Show Edit Form for the specified resource.
    *
    * @param Request $request
    * @param Absent $absent
    * @return \Inertia\Response|\Illuminate\Http\RedirectResponse
    */
    public function edit(Request $request, Absent $absent)
    {
        try {
            $this->authorize('update', $absent);
            //Fetch relationships
            



        $absent->load([
            'session',
            'student',
        ]);
                        return Inertia::render("Absents/Edit", ["model" => $absent]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Update the specified resource in storage.
    *
    * @param UpdateAbsent $request
    * @param {$modelBaseName} $absent
    * @return \Illuminate\Http\RedirectResponse
    */
    public function update(UpdateAbsent $request, Absent $absent)
    {
        try {
            $data = $request->sanitizedObject();
            $res = $this->repo::init($absent)->update($data);
            return back()->with(['success' => "The Absent was updated succesfully."]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param Absent $absent
    * @return \Illuminate\Http\RedirectResponse
    */
    public function destroy(DestroyAbsent $request, Absent $absent)
    {
        $res = $this->repo::init($absent)->destroy();
        if ($res) {
            return back()->with(['success' => "The Absent was deleted succesfully."]);
        }
        else {
            return back()->with(['error' => "The Absent could not be deleted."]);
        }
    }
}
