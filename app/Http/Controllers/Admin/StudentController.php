<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\Student\IndexStudent;
use App\Http\Requests\Student\StoreStudent;
use App\Http\Requests\Student\UpdateStudent;
use App\Http\Requests\Student\DestroyStudent;
use App\Models\Student;
use App\Repositories\Students;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Inertia\Inertia;
use Yajra\DataTables\Html\Column;

class StudentController  extends Controller
{
    private Students $repo;
    public function __construct(Students $repo)
    {
        $this->repo = $repo;
    }

    /**
    * Display a listing of the resource.
    *
    * @param  Request $request
    * @return    \Inertia\Response
    * @throws  \Illuminate\Auth\Access\AuthorizationException
    */
    public function index(Request $request): \Inertia\Response
    {
        $this->authorize('viewAny', Student::class);
        return Inertia::render('Students/Index',[
            "can" => [
                "viewAny" => \Auth::user()->can('viewAny', Student::class),
                "create" => \Auth::user()->can('create', Student::class),
            ],
            "columns" => $this->repo::dtColumns(),
        ]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return  \Inertia\Response
    */
    public function create()
    {
        $this->authorize('create', Student::class);
        return Inertia::render("Students/Create",[
            "can" => [
            "viewAny" => \Auth::user()->can('viewAny', Student::class),
            "create" => \Auth::user()->can('create', Student::class),
            ]
        ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param StoreStudent $request
    * @return \Illuminate\Http\RedirectResponse
    */
    public function store(StoreStudent $request)
    {
        try {
            $data = $request->sanitizedObject();
            $student = $this->repo::store($data);
            return back()->with(['success' => "The Student was created succesfully."]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Display the specified resource.
    *
    * @param Request $request
    * @param Student $student
    * @return \Inertia\Response|\Illuminate\Http\RedirectResponse
    */
    public function show(Request $request, Student $student)
    {
        try {
            $this->authorize('view', $student);
            $model = $this->repo::init($student)->show($request);
            return Inertia::render("Students/Show", ["model" => $model]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Show Edit Form for the specified resource.
    *
    * @param Request $request
    * @param Student $student
    * @return \Inertia\Response|\Illuminate\Http\RedirectResponse
    */
    public function edit(Request $request, Student $student)
    {
        try {
            $this->authorize('update', $student);
            //Fetch relationships
            



        $student->load([
            'group',
        ]);
                        return Inertia::render("Students/Edit", ["model" => $student]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Update the specified resource in storage.
    *
    * @param UpdateStudent $request
    * @param {$modelBaseName} $student
    * @return \Illuminate\Http\RedirectResponse
    */
    public function update(UpdateStudent $request, Student $student)
    {
        try {
            $data = $request->sanitizedObject();
            $res = $this->repo::init($student)->update($data);
            return back()->with(['success' => "The Student was updated succesfully."]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param Student $student
    * @return \Illuminate\Http\RedirectResponse
    */
    public function destroy(DestroyStudent $request, Student $student)
    {
        $res = $this->repo::init($student)->destroy();
        if ($res) {
            return back()->with(['success' => "The Student was deleted succesfully."]);
        }
        else {
            return back()->with(['error' => "The Student could not be deleted."]);
        }
    }
}
