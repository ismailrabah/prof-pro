<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\Event\IndexEvent;
use App\Http\Requests\Event\StoreEvent;
use App\Http\Requests\Event\UpdateEvent;
use App\Http\Requests\Event\DestroyEvent;
use App\Models\Event;
use App\Repositories\Events;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Inertia\Inertia;
use Yajra\DataTables\Html\Column;

class EventController  extends Controller
{
    private Events $repo;
    public function __construct(Events $repo)
    {
        $this->repo = $repo;
    }

    /**
    * Display a listing of the resource.
    *
    * @param  Request $request
    * @return    \Inertia\Response
    * @throws  \Illuminate\Auth\Access\AuthorizationException
    */
    public function index(Request $request): \Inertia\Response
    {
        $this->authorize('viewAny', Event::class);
        return Inertia::render('Events/Index',[
            "can" => [
                "viewAny" => \Auth::user()->can('viewAny', Event::class),
                "create" => \Auth::user()->can('create', Event::class),
            ],
            "columns" => $this->repo::dtColumns(),
        ]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return  \Inertia\Response
    */
    public function create()
    {
        $this->authorize('create', Event::class);
        return Inertia::render("Events/Create",[
            "can" => [
            "viewAny" => \Auth::user()->can('viewAny', Event::class),
            "create" => \Auth::user()->can('create', Event::class),
            ]
        ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param StoreEvent $request
    * @return \Illuminate\Http\RedirectResponse
    */
    public function store(StoreEvent $request)
    {
        try {
            $data = $request->sanitizedObject();
            $event = $this->repo::store($data);
            return back()->with(['success' => "The Event was created succesfully."]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Display the specified resource.
    *
    * @param Request $request
    * @param Event $event
    * @return \Inertia\Response|\Illuminate\Http\RedirectResponse
    */
    public function show(Request $request, Event $event)
    {
        try {
            $this->authorize('view', $event);
            $model = $this->repo::init($event)->show($request);
            return Inertia::render("Events/Show", ["model" => $model]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Show Edit Form for the specified resource.
    *
    * @param Request $request
    * @param Event $event
    * @return \Inertia\Response|\Illuminate\Http\RedirectResponse
    */
    public function edit(Request $request, Event $event)
    {
        try {
            $this->authorize('update', $event);
            //Fetch relationships
            

                        return Inertia::render("Events/Edit", ["model" => $event]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Update the specified resource in storage.
    *
    * @param UpdateEvent $request
    * @param {$modelBaseName} $event
    * @return \Illuminate\Http\RedirectResponse
    */
    public function update(UpdateEvent $request, Event $event)
    {
        try {
            $data = $request->sanitizedObject();
            $res = $this->repo::init($event)->update($data);
            return back()->with(['success' => "The Event was updated succesfully."]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param Event $event
    * @return \Illuminate\Http\RedirectResponse
    */
    public function destroy(DestroyEvent $request, Event $event)
    {
        $res = $this->repo::init($event)->destroy();
        if ($res) {
            return back()->with(['success' => "The Event was deleted succesfully."]);
        }
        else {
            return back()->with(['error' => "The Event could not be deleted."]);
        }
    }
}
