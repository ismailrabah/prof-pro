<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\SessionType\IndexSessionType;
use App\Http\Requests\SessionType\StoreSessionType;
use App\Http\Requests\SessionType\UpdateSessionType;
use App\Http\Requests\SessionType\DestroySessionType;
use App\Models\SessionType;
use App\Repositories\SessionTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Inertia\Inertia;
use Yajra\DataTables\Html\Column;

class SessionTypeController  extends Controller
{
    private SessionTypes $repo;
    public function __construct(SessionTypes $repo)
    {
        $this->repo = $repo;
    }

    /**
    * Display a listing of the resource.
    *
    * @param  Request $request
    * @return    \Inertia\Response
    * @throws  \Illuminate\Auth\Access\AuthorizationException
    */
    public function index(Request $request): \Inertia\Response
    {
        $this->authorize('viewAny', SessionType::class);
        return Inertia::render('SessionTypes/Index',[
            "can" => [
                "viewAny" => \Auth::user()->can('viewAny', SessionType::class),
                "create" => \Auth::user()->can('create', SessionType::class),
            ],
            "columns" => $this->repo::dtColumns(),
        ]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return  \Inertia\Response
    */
    public function create()
    {
        $this->authorize('create', SessionType::class);
        return Inertia::render("SessionTypes/Create",[
            "can" => [
            "viewAny" => \Auth::user()->can('viewAny', SessionType::class),
            "create" => \Auth::user()->can('create', SessionType::class),
            ]
        ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param StoreSessionType $request
    * @return \Illuminate\Http\RedirectResponse
    */
    public function store(StoreSessionType $request)
    {
        try {
            $data = $request->sanitizedObject();
            $sessionType = $this->repo::store($data);
            return back()->with(['success' => "The Session Type was created succesfully."]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Display the specified resource.
    *
    * @param Request $request
    * @param SessionType $sessionType
    * @return \Inertia\Response|\Illuminate\Http\RedirectResponse
    */
    public function show(Request $request, SessionType $sessionType)
    {
        try {
            $this->authorize('view', $sessionType);
            $model = $this->repo::init($sessionType)->show($request);
            return Inertia::render("SessionTypes/Show", ["model" => $model]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Show Edit Form for the specified resource.
    *
    * @param Request $request
    * @param SessionType $sessionType
    * @return \Inertia\Response|\Illuminate\Http\RedirectResponse
    */
    public function edit(Request $request, SessionType $sessionType)
    {
        try {
            $this->authorize('update', $sessionType);
            //Fetch relationships
            

                        return Inertia::render("SessionTypes/Edit", ["model" => $sessionType]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Update the specified resource in storage.
    *
    * @param UpdateSessionType $request
    * @param {$modelBaseName} $sessionType
    * @return \Illuminate\Http\RedirectResponse
    */
    public function update(UpdateSessionType $request, SessionType $sessionType)
    {
        try {
            $data = $request->sanitizedObject();
            $res = $this->repo::init($sessionType)->update($data);
            return back()->with(['success' => "The SessionType was updated succesfully."]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param SessionType $sessionType
    * @return \Illuminate\Http\RedirectResponse
    */
    public function destroy(DestroySessionType $request, SessionType $sessionType)
    {
        $res = $this->repo::init($sessionType)->destroy();
        if ($res) {
            return back()->with(['success' => "The SessionType was deleted succesfully."]);
        }
        else {
            return back()->with(['error' => "The SessionType could not be deleted."]);
        }
    }
}
