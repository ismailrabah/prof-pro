<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\Group\IndexGroup;
use App\Http\Requests\Group\StoreGroup;
use App\Http\Requests\Group\UpdateGroup;
use App\Http\Requests\Group\DestroyGroup;
use App\Models\Group;
use App\Repositories\Groups;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Inertia\Inertia;
use Yajra\DataTables\Html\Column;

class GroupController  extends Controller
{
    private Groups $repo;
    public function __construct(Groups $repo)
    {
        $this->repo = $repo;
    }

    /**
    * Display a listing of the resource.
    *
    * @param  Request $request
    * @return    \Inertia\Response
    * @throws  \Illuminate\Auth\Access\AuthorizationException
    */
    public function index(Request $request): \Inertia\Response
    {
        $this->authorize('viewAny', Group::class);
        return Inertia::render('Groups/Index',[
            "can" => [
                "viewAny" => \Auth::user()->can('viewAny', Group::class),
                "create" => \Auth::user()->can('create', Group::class),
            ],
            "columns" => $this->repo::dtColumns(),
        ]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return  \Inertia\Response
    */
    public function create()
    {
        $this->authorize('create', Group::class);
        return Inertia::render("Groups/Create",[
            "can" => [
            "viewAny" => \Auth::user()->can('viewAny', Group::class),
            "create" => \Auth::user()->can('create', Group::class),
            ]
        ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param StoreGroup $request
    * @return \Illuminate\Http\RedirectResponse
    */
    public function store(StoreGroup $request)
    {
        try {
            $data = $request->sanitizedObject();
            $group = $this->repo::store($data);
            return back()->with(['success' => "The Group was created succesfully."]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Display the specified resource.
    *
    * @param Request $request
    * @param Group $group
    * @return \Inertia\Response|\Illuminate\Http\RedirectResponse
    */
    public function show(Request $request, Group $group)
    {
        try {
            $this->authorize('view', $group);
            $model = $this->repo::init($group)->show($request);
            return Inertia::render("Groups/Show", ["model" => $model]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Show Edit Form for the specified resource.
    *
    * @param Request $request
    * @param Group $group
    * @return \Inertia\Response|\Illuminate\Http\RedirectResponse
    */
    public function edit(Request $request, Group $group)
    {
        try {
            $this->authorize('update', $group);
            //Fetch relationships
            

                        return Inertia::render("Groups/Edit", ["model" => $group]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Update the specified resource in storage.
    *
    * @param UpdateGroup $request
    * @param {$modelBaseName} $group
    * @return \Illuminate\Http\RedirectResponse
    */
    public function update(UpdateGroup $request, Group $group)
    {
        try {
            $data = $request->sanitizedObject();
            $res = $this->repo::init($group)->update($data);
            return back()->with(['success' => "The Group was updated succesfully."]);
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return back()->with([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param Group $group
    * @return \Illuminate\Http\RedirectResponse
    */
    public function destroy(DestroyGroup $request, Group $group)
    {
        $res = $this->repo::init($group)->destroy();
        if ($res) {
            return back()->with(['success' => "The Group was deleted succesfully."]);
        }
        else {
            return back()->with(['error' => "The Group could not be deleted."]);
        }
    }
}
