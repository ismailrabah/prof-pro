<?php
namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use App\Http\Requests\Session\IndexSession;
use App\Http\Requests\Session\StoreSession;
use App\Http\Requests\Session\UpdateSession;
use App\Http\Requests\Session\DestroySession;
use App\Models\Session;
use App\Repositories\Sessions;
use Illuminate\Http\Request;
use Savannabits\JetstreamInertiaGenerator\Helpers\ApiResponse;
use Savannabits\Pagetables\Column;
use Savannabits\Pagetables\Pagetables;
use Yajra\DataTables\DataTables;

class SessionController  extends Controller
{
    private ApiResponse $api;
    private Sessions $repo;
    public function __construct(ApiResponse $apiResponse, Sessions $repo)
    {
        $this->api = $apiResponse;
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource (paginated).
     * @return columnsToQuery \Illuminate\Http\JsonResponse
     */
    public function index(IndexSession $request)
    {
        $query = Session::query(); // You can extend this however you want.
        $cols = [
            Column::name('id')->title('Id')->sort()->searchable(),
            Column::name('start_date')->title('Start Date')->sort()->searchable(),
            Column::name('end_date')->title('End Date')->sort()->searchable(),
            Column::name('updated_at')->title('Updated At')->sort()->searchable(),
            
            Column::name('actions')->title('')->raw()
        ];
        $data = Pagetables::of($query)->columns($cols)->make(true);
        return $this->api->success()->message("List of Sessions")->payload($data)->send();
    }

    public function dt(Request $request) {
        $query = Session::query()->select(Session::getModel()->getTable().'.*'); // You can extend this however you want.
        return $this->repo::dt($query);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param StoreSession $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreSession $request)
    {
        try {
            $data = $request->sanitizedObject();
            $session = $this->repo::store($data);
            return $this->api->success()->message('Session Created')->payload($session)->send();
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return $this->api->failed()->message($exception->getMessage())->payload([])->code(500)->send();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Session $session
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, Session $session)
    {
        try {
            $payload = $this->repo::init($session)->show($request);
            return $this->api->success()
                        ->message("Session $session->id")
                        ->payload($payload)->send();
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return $this->api->failed()->message($exception->getMessage())->send();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateSession $request
     * @param {$modelBaseName} $session
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateSession $request, Session $session)
    {
        try {
            $data = $request->sanitizedObject();
            $res = $this->repo::init($session)->update($data);
            return $this->api->success()->message("Session has been updated")->payload($res)->code(200)->send();
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return $this->api->failed()->code(400)->message($exception->getMessage())->send();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Session $session
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(DestroySession $request, Session $session)
    {
        $res = $this->repo::init($session)->destroy();
        return $this->api->success()->message("Session has been deleted")->payload($res)->code(200)->send();
    }

}
