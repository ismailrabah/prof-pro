<?php
namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use App\Http\Requests\Student\IndexStudent;
use App\Http\Requests\Student\StoreStudent;
use App\Http\Requests\Student\UpdateStudent;
use App\Http\Requests\Student\DestroyStudent;
use App\Models\Student;
use App\Repositories\Students;
use Illuminate\Http\Request;
use Savannabits\JetstreamInertiaGenerator\Helpers\ApiResponse;
use Savannabits\Pagetables\Column;
use Savannabits\Pagetables\Pagetables;
use Yajra\DataTables\DataTables;

class StudentController  extends Controller
{
    private ApiResponse $api;
    private Students $repo;
    public function __construct(ApiResponse $apiResponse, Students $repo)
    {
        $this->api = $apiResponse;
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource (paginated).
     * @return columnsToQuery \Illuminate\Http\JsonResponse
     */
    public function index(IndexStudent $request)
    {
        $query = Student::query(); // You can extend this however you want.
        $cols = [
            Column::name('id')->title('Id')->sort()->searchable(),
            Column::name('name')->title('Name')->sort()->searchable(),
            Column::name('email')->title('Email')->sort()->searchable(),
            Column::name('phone')->title('Phone')->sort()->searchable(),
            Column::name('birthday')->title('Birthday')->sort()->searchable(),
            Column::name('updated_at')->title('Updated At')->sort()->searchable(),
            
            Column::name('actions')->title('')->raw()
        ];
        $data = Pagetables::of($query)->columns($cols)->make(true);
        return $this->api->success()->message("List of Students")->payload($data)->send();
    }

    public function dt(Request $request) {
        $query = Student::query()->select(Student::getModel()->getTable().'.*'); // You can extend this however you want.
        return $this->repo::dt($query);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param StoreStudent $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreStudent $request)
    {
        try {
            $data = $request->sanitizedObject();
            $student = $this->repo::store($data);
            return $this->api->success()->message('Student Created')->payload($student)->send();
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return $this->api->failed()->message($exception->getMessage())->payload([])->code(500)->send();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Student $student
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, Student $student)
    {
        try {
            $payload = $this->repo::init($student)->show($request);
            return $this->api->success()
                        ->message("Student $student->id")
                        ->payload($payload)->send();
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return $this->api->failed()->message($exception->getMessage())->send();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateStudent $request
     * @param {$modelBaseName} $student
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateStudent $request, Student $student)
    {
        try {
            $data = $request->sanitizedObject();
            $res = $this->repo::init($student)->update($data);
            return $this->api->success()->message("Student has been updated")->payload($res)->code(200)->send();
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return $this->api->failed()->code(400)->message($exception->getMessage())->send();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Student $student
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(DestroyStudent $request, Student $student)
    {
        $res = $this->repo::init($student)->destroy();
        return $this->api->success()->message("Student has been deleted")->payload($res)->code(200)->send();
    }

}
