<?php
namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use App\Http\Requests\Event\IndexEvent;
use App\Http\Requests\Event\StoreEvent;
use App\Http\Requests\Event\UpdateEvent;
use App\Http\Requests\Event\DestroyEvent;
use App\Models\Event;
use App\Repositories\Events;
use Illuminate\Http\Request;
use Savannabits\JetstreamInertiaGenerator\Helpers\ApiResponse;
use Savannabits\Pagetables\Column;
use Savannabits\Pagetables\Pagetables;
use Yajra\DataTables\DataTables;

class EventController  extends Controller
{
    private ApiResponse $api;
    private Events $repo;
    public function __construct(ApiResponse $apiResponse, Events $repo)
    {
        $this->api = $apiResponse;
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource (paginated).
     * @return columnsToQuery \Illuminate\Http\JsonResponse
     */
    public function index(IndexEvent $request)
    {
        $query = Event::query(); // You can extend this however you want.
        $cols = [
            
            Column::name('actions')->title('')->raw()
        ];
        $data = Pagetables::of($query)->columns($cols)->make(true);
        return $this->api->success()->message("List of Events")->payload($data)->send();
    }

    public function dt(Request $request) {
        $query = Event::query()->select(Event::getModel()->getTable().'.*'); // You can extend this however you want.
        return $this->repo::dt($query);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param StoreEvent $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreEvent $request)
    {
        try {
            $data = $request->sanitizedObject();
            $event = $this->repo::store($data);
            return $this->api->success()->message('Event Created')->payload($event)->send();
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return $this->api->failed()->message($exception->getMessage())->payload([])->code(500)->send();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Event $event
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, Event $event)
    {
        try {
            $payload = $this->repo::init($event)->show($request);
            return $this->api->success()
                        ->message("Event $event->id")
                        ->payload($payload)->send();
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return $this->api->failed()->message($exception->getMessage())->send();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateEvent $request
     * @param {$modelBaseName} $event
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateEvent $request, Event $event)
    {
        try {
            $data = $request->sanitizedObject();
            $res = $this->repo::init($event)->update($data);
            return $this->api->success()->message("Event has been updated")->payload($res)->code(200)->send();
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return $this->api->failed()->code(400)->message($exception->getMessage())->send();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Event $event
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(DestroyEvent $request, Event $event)
    {
        $res = $this->repo::init($event)->destroy();
        return $this->api->success()->message("Event has been deleted")->payload($res)->code(200)->send();
    }

}
