<?php
namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use App\Http\Requests\Group\IndexGroup;
use App\Http\Requests\Group\StoreGroup;
use App\Http\Requests\Group\UpdateGroup;
use App\Http\Requests\Group\DestroyGroup;
use App\Models\Group;
use App\Repositories\Groups;
use Illuminate\Http\Request;
use Savannabits\JetstreamInertiaGenerator\Helpers\ApiResponse;
use Savannabits\Pagetables\Column;
use Savannabits\Pagetables\Pagetables;
use Yajra\DataTables\DataTables;

class GroupController  extends Controller
{
    private ApiResponse $api;
    private Groups $repo;
    public function __construct(ApiResponse $apiResponse, Groups $repo)
    {
        $this->api = $apiResponse;
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource (paginated).
     * @return columnsToQuery \Illuminate\Http\JsonResponse
     */
    public function index(IndexGroup $request)
    {
        $query = Group::query(); // You can extend this however you want.
        $cols = [
            Column::name('id')->title('Id')->sort()->searchable(),
            Column::name('name')->title('Name')->sort()->searchable(),
            Column::name('level')->title('level')->sort()->searchable(),
            Column::name('total_members')->title('Total Members')->sort()->searchable(),
            Column::name('updated_at')->title('Updated At')->sort()->searchable(),
            Column::name('actions')->title('')->raw()
        ];
        $data = Pagetables::of($query)->columns($cols)->make(true);
        return $this->api->success()->message("List of Groups")->payload($data)->send();
    }

    public function dt(Request $request) {
        $query = Group::query()->select(Group::getModel()->getTable().'.*'); // You can extend this however you want.
        return $this->repo::dt($query);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param StoreGroup $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreGroup $request)
    {
        try {
            $data = $request->sanitizedObject();
            $group = $this->repo::store($data);
            return $this->api->success()->message('Group Created')->payload($group)->send();
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return $this->api->failed()->message($exception->getMessage())->payload([])->code(500)->send();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Group $group
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, Group $group)
    {
        try {
            $payload = $this->repo::init($group)->show($request);
            return $this->api->success()
                        ->message("Group $group->id")
                        ->payload($payload)->send();
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return $this->api->failed()->message($exception->getMessage())->send();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateGroup $request
     * @param {$modelBaseName} $group
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateGroup $request, Group $group)
    {
        try {
            $data = $request->sanitizedObject();
            $res = $this->repo::init($group)->update($data);
            return $this->api->success()->message("Group has been updated")->payload($res)->code(200)->send();
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return $this->api->failed()->code(400)->message($exception->getMessage())->send();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Group $group
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(DestroyGroup $request, Group $group)
    {
        $res = $this->repo::init($group)->destroy();
        return $this->api->success()->message("Group has been deleted")->payload($res)->code(200)->send();
    }

}
