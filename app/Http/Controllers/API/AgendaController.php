<?php
namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use App\Repositories\Absents;
use App\Repositories\Events;
use App\Repositories\Sessions;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Savannabits\JetstreamInertiaGenerator\Helpers\ApiResponse;
use Barryvdh\DomPDF\Facade\Pdf;

class AgendaController  extends Controller
{
    private ApiResponse $api;
    private Sessions $session_repo;
    private Events $event_repo;
    private Absents $absent_repo;

    public function __construct(ApiResponse $apiResponse,Sessions $s_repo , Events $e_repo , Absents $a_repo)
    {
        $this->api = $apiResponse;
        
        $this->session_repo = $s_repo;
        $this->event_repo = $e_repo;
        $this->absent_repo = $a_repo;
    }

    /**
     * Display a listing of the resource (paginated).
     * @return columnsToQuery \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $displayPeriodUom = $request->request->get('displayPeriodUom');
        $displayPeriodCount = $request->request->get('displayPeriodCount');
        $start_date = $request->request->get('start_date');
        $user = $request->request->get('user');
        $group = $request->request->get('group');
        $module = $request->request->get('module');
        $end_date = $this->session_repo::handlerAgendaPeriod($displayPeriodUom , $displayPeriodCount , $start_date );
        $start_date = Carbon::createFromFormat('m/d/Y',  $start_date);

        $sessions= $this->session_repo::agenda_event( $start_date , $end_date , $user , $group , $module);
        $events = $this->event_repo::agenda_event( $start_date , $end_date );

        $data['items'] = array_merge($sessions , $events);

        return $this->api->success()->message("List of Sessions")->payload($data)->send();
       
    }

    /**
     * Print agenda of one week filter by user , groupe and module 
     */
    public function print(Request $request){
        
        try {

            $param = $request->all();
            $result= $this->session_repo::week_wessions($param);

            $pdf = PDF::loadView('pdf.agenda', 
                [
                    'emploi_type'       => $result['emploi_type'], 
                    'sessions'          => $result['sessions'], 
                    'mass_houres'       => $result['mass_houres'], 
                    'group'             => $result['group'], 
                    'prof'             => $result['prof'], 
                    'yearOfFormation'   => $this->session_repo::yearOfFormation(), 
                    'formation_mode'    => '.', 
                ]
            );
            $pdf->setPaper('a4', 'landscape')->setWarnings(true);
            return $pdf->stream('agenda.pdf');

        } catch (\Throwable $exception) {
            \Log::error($exception);
            return $this->api->failed()->message($exception->getMessage())->payload([])->code(500)->send();
        }
    }

}
