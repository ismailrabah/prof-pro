<?php
namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use App\Http\Requests\Absent\IndexAbsent;
use App\Http\Requests\Absent\StoreAbsent;
use App\Http\Requests\Absent\UpdateAbsent;
use App\Http\Requests\Absent\DestroyAbsent;
use App\Models\Absent;
use App\Repositories\Absents;
use Illuminate\Http\Request;
use Savannabits\JetstreamInertiaGenerator\Helpers\ApiResponse;
use Savannabits\Pagetables\Column;
use Savannabits\Pagetables\Pagetables;
use Yajra\DataTables\DataTables;

class AbsentController  extends Controller
{
    private ApiResponse $api;
    private Absents $repo;
    public function __construct(ApiResponse $apiResponse, Absents $repo)
    {
        $this->api = $apiResponse;
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource (paginated).
     * @return columnsToQuery \Illuminate\Http\JsonResponse
     */
    public function index(IndexAbsent $request)
    {
        $query = Absent::query(); // You can extend this however you want.
        $cols = [
            Column::name('id')->title('Id')->sort()->searchable(),
            Column::name('reason')->title('Reason')->sort()->searchable(),
            Column::name('updated_at')->title('Updated At')->sort()->searchable(),
            
            Column::name('actions')->title('')->raw()
        ];
        $data = Pagetables::of($query)->columns($cols)->make(true);
        return $this->api->success()->message("List of Absents")->payload($data)->send();
    }

    public function dt(Request $request) {
        $query = Absent::query()->select(Absent::getModel()->getTable().'.*'); // You can extend this however you want.
        return $this->repo::dt($query);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAbsent $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreAbsent $request)
    {
        try {
            $data = $request->sanitizedObject();
            $absent = $this->repo::store($data);
            return $this->api->success()->message('Absent Created')->payload($absent)->send();
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return $this->api->failed()->message($exception->getMessage())->payload([])->code(500)->send();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Absent $absent
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, Absent $absent)
    {
        try {
            $payload = $this->repo::init($absent)->show($request);
            return $this->api->success()
                        ->message("Absent $absent->id")
                        ->payload($payload)->send();
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return $this->api->failed()->message($exception->getMessage())->send();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateAbsent $request
     * @param {$modelBaseName} $absent
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateAbsent $request, Absent $absent)
    {
        try {
            $data = $request->sanitizedObject();
            $res = $this->repo::init($absent)->update($data);
            return $this->api->success()->message("Absent has been updated")->payload($res)->code(200)->send();
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return $this->api->failed()->code(400)->message($exception->getMessage())->send();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Absent $absent
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(DestroyAbsent $request, Absent $absent)
    {
        $res = $this->repo::init($absent)->destroy();
        return $this->api->success()->message("Absent has been deleted")->payload($res)->code(200)->send();
    }

}
