<?php
namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use App\Http\Requests\SessionType\IndexSessionType;
use App\Http\Requests\SessionType\StoreSessionType;
use App\Http\Requests\SessionType\UpdateSessionType;
use App\Http\Requests\SessionType\DestroySessionType;
use App\Models\SessionType;
use App\Repositories\SessionTypes;
use Illuminate\Http\Request;
use Savannabits\JetstreamInertiaGenerator\Helpers\ApiResponse;
use Savannabits\Pagetables\Column;
use Savannabits\Pagetables\Pagetables;
use Yajra\DataTables\DataTables;

class SessionTypeController  extends Controller
{
    private ApiResponse $api;
    private SessionTypes $repo;
    public function __construct(ApiResponse $apiResponse, SessionTypes $repo)
    {
        $this->api = $apiResponse;
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource (paginated).
     * @return columnsToQuery \Illuminate\Http\JsonResponse
     */
    public function index(IndexSessionType $request)
    {
        $query = SessionType::query(); // You can extend this however you want.
        $cols = [
            Column::name('type')->title('Type')->sort()->searchable(),
            Column::name('color')->title('Color')->sort()->searchable(),
            
            Column::name('actions')->title('')->raw()
        ];
        $data = Pagetables::of($query)->columns($cols)->make(true);
        return $this->api->success()->message("List of SessionTypes")->payload($data)->send();
    }

    public function dt(Request $request) {
        $query = SessionType::query()->select(SessionType::getModel()->getTable().'.*'); // You can extend this however you want.
        return $this->repo::dt($query);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param StoreSessionType $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreSessionType $request)
    {
        try {
            $data = $request->sanitizedObject();
            $sessionType = $this->repo::store($data);
            return $this->api->success()->message('Session Type Created')->payload($sessionType)->send();
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return $this->api->failed()->message($exception->getMessage())->payload([])->code(500)->send();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param SessionType $sessionType
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, SessionType $sessionType)
    {
        try {
            $payload = $this->repo::init($sessionType)->show($request);
            return $this->api->success()
                        ->message("Session Type $sessionType->id")
                        ->payload($payload)->send();
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return $this->api->failed()->message($exception->getMessage())->send();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateSessionType $request
     * @param {$modelBaseName} $sessionType
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateSessionType $request, SessionType $sessionType)
    {
        try {
            $data = $request->sanitizedObject();
            $res = $this->repo::init($sessionType)->update($data);
            return $this->api->success()->message("Session Type has been updated")->payload($res)->code(200)->send();
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return $this->api->failed()->code(400)->message($exception->getMessage())->send();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param SessionType $sessionType
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(DestroySessionType $request, SessionType $sessionType)
    {
        $res = $this->repo::init($sessionType)->destroy();
        return $this->api->success()->message("Session Type has been deleted")->payload($res)->code(200)->send();
    }

}
