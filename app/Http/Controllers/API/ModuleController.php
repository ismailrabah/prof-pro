<?php
namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use App\Http\Requests\Module\IndexModule;
use App\Http\Requests\Module\StoreModule;
use App\Http\Requests\Module\UpdateModule;
use App\Http\Requests\Module\DestroyModule;
use App\Models\Module;
use App\Repositories\Modules;
use Illuminate\Http\Request;
use Savannabits\JetstreamInertiaGenerator\Helpers\ApiResponse;
use Savannabits\Pagetables\Column;
use Savannabits\Pagetables\Pagetables;
use Yajra\DataTables\DataTables;

class ModuleController  extends Controller
{
    private ApiResponse $api;
    private Modules $repo;
    public function __construct(ApiResponse $apiResponse, Modules $repo)
    {
        $this->api = $apiResponse;
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource (paginated).
     * @return columnsToQuery \Illuminate\Http\JsonResponse
     */
    public function index(IndexModule $request)
    {
        $query = Module::query(); // You can extend this however you want.
        $cols = [
            Column::name('id')->title('Id')->sort()->searchable(),
            Column::name('name')->title('Name')->sort()->searchable(),
            Column::name('level')->title('Level')->sort()->searchable(),
            Column::name('average')->title('Average')->sort()->searchable(),
            Column::name('start_date')->title('Start Date')->sort()->searchable(),
            Column::name('end_date')->title('End Date')->sort()->searchable(),
            Column::name('total_hours')->title('Total Hours')->sort()->searchable(),
            Column::name('updated_at')->title('Updated At')->sort()->searchable(),
            
            Column::name('actions')->title('')->raw()
        ];
        $data = Pagetables::of($query)->columns($cols)->make(true);
        return $this->api->success()->message("List of Modules")->payload($data)->send();
    }

    public function dt(Request $request) {
        $query = Module::query()->select(Module::getModel()->getTable().'.*'); // You can extend this however you want.
        return $this->repo::dt($query);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param StoreModule $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreModule $request)
    {
        try {
            $data = $request->sanitizedObject();
            $module = $this->repo::store($data);
            return $this->api->success()->message('Module Created')->payload($module)->send();
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return $this->api->failed()->message($exception->getMessage())->payload([])->code(500)->send();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Module $module
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, Module $module)
    {
        try {
            $payload = $this->repo::init($module)->show($request);
            return $this->api->success()
                        ->message("Module $module->id")
                        ->payload($payload)->send();
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return $this->api->failed()->message($exception->getMessage())->send();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateModule $request
     * @param {$modelBaseName} $module
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateModule $request, Module $module)
    {
        try {
            $data = $request->sanitizedObject();
            $res = $this->repo::init($module)->update($data);
            return $this->api->success()->message("Module has been updated")->payload($res)->code(200)->send();
        } catch (\Throwable $exception) {
            \Log::error($exception);
            return $this->api->failed()->code(400)->message($exception->getMessage())->send();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Module $module
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(DestroyModule $request, Module $module)
    {
        $res = $this->repo::init($module)->destroy();
        return $this->api->success()->message("Module has been deleted")->payload($res)->code(200)->send();
    }

}
