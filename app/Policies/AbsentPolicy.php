<?php
namespace App\Policies;

use App\Models\Absent;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AbsentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param    \App\Models\User  $user
     * @return  mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('absents.index');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param    \App\Models\User  $user
     * @param    Absent  $absent
     * @return  mixed
     */
    public function view(User $user, Absent $absent)
    {
        return $user->hasPermissionTo('absents.show');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param    \App\Models\User  $user
     * @return  mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('absents.create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param    \App\Models\User  $user
     * @param    Absent  $absent
     * @return  mixed
     */
    public function update(User $user, Absent $absent)
    {
        return $user->hasPermissionTo('absents.edit');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param    \App\Models\User  $user
     * @param    Absent  $absent
     * @return  mixed
     */
    public function delete(User $user, Absent $absent)
    {
        return $user->hasPermissionTo('absents.delete');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param    \App\Models\User  $user
     * @param    Absent  $absent
     * @return  mixed
     */
    public function restore(User $user, Absent $absent)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param    \App\Models\User  $user
     * @param    Absent  $absent
     * @return  mixed
     */
    public function forceDelete(User $user, Absent $absent)
    {
        return $user->hasPermissionTo('absents.delete');
    }
}
