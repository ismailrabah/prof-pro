<?php
namespace App\Policies;

use App\Models\SessionType;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SessionTypePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param    \App\Models\User  $user
     * @return  mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('session-types.index');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param    \App\Models\User  $user
     * @param    SessionType  $sessionType
     * @return  mixed
     */
    public function view(User $user, SessionType $sessionType)
    {
        return $user->hasPermissionTo('session-types.show');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param    \App\Models\User  $user
     * @return  mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('session-types.create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param    \App\Models\User  $user
     * @param    SessionType  $sessionType
     * @return  mixed
     */
    public function update(User $user, SessionType $sessionType)
    {
        return $user->hasPermissionTo('session-types.edit');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param    \App\Models\User  $user
     * @param    SessionType  $sessionType
     * @return  mixed
     */
    public function delete(User $user, SessionType $sessionType)
    {
        return $user->hasPermissionTo('session-types.delete');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param    \App\Models\User  $user
     * @param    SessionType  $sessionType
     * @return  mixed
     */
    public function restore(User $user, SessionType $sessionType)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param    \App\Models\User  $user
     * @param    SessionType  $sessionType
     * @return  mixed
     */
    public function forceDelete(User $user, SessionType $sessionType)
    {
        return $user->hasPermissionTo('session-types.delete');
    }
}
