<?php
namespace App\Policies;

use App\Models\Module;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ModulePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param    \App\Models\User  $user
     * @return  mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('modules.index');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param    \App\Models\User  $user
     * @param    Module  $module
     * @return  mixed
     */
    public function view(User $user, Module $module)
    {
        return $user->hasPermissionTo('modules.show');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param    \App\Models\User  $user
     * @return  mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('modules.create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param    \App\Models\User  $user
     * @param    Module  $module
     * @return  mixed
     */
    public function update(User $user, Module $module)
    {
        return $user->hasPermissionTo('modules.edit');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param    \App\Models\User  $user
     * @param    Module  $module
     * @return  mixed
     */
    public function delete(User $user, Module $module)
    {
        return $user->hasPermissionTo('modules.delete');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param    \App\Models\User  $user
     * @param    Module  $module
     * @return  mixed
     */
    public function restore(User $user, Module $module)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param    \App\Models\User  $user
     * @param    Module  $module
     * @return  mixed
     */
    public function forceDelete(User $user, Module $module)
    {
        return $user->hasPermissionTo('modules.delete');
    }
}
