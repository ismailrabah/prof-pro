<?php
namespace App\Policies;

use App\Models\Session;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SessionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param    \App\Models\User  $user
     * @return  mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('sessions.index');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param    \App\Models\User  $user
     * @param    Session  $session
     * @return  mixed
     */
    public function view(User $user, Session $session)
    {
        return $user->hasPermissionTo('sessions.show');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param    \App\Models\User  $user
     * @return  mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('sessions.create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param    \App\Models\User  $user
     * @param    Session  $session
     * @return  mixed
     */
    public function update(User $user, Session $session)
    {
        return $user->hasPermissionTo('sessions.edit');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param    \App\Models\User  $user
     * @param    Session  $session
     * @return  mixed
     */
    public function delete(User $user, Session $session)
    {
        return $user->hasPermissionTo('sessions.delete');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param    \App\Models\User  $user
     * @param    Session  $session
     * @return  mixed
     */
    public function restore(User $user, Session $session)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param    \App\Models\User  $user
     * @param    Session  $session
     * @return  mixed
     */
    public function forceDelete(User $user, Session $session)
    {
        return $user->hasPermissionTo('sessions.delete');
    }
}
