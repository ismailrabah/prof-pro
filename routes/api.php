<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


/* Auto-generated users api routes */
Route::group(["middleware"=>['auth:sanctum', 'verified'],'as' => 'api.'], function () {

    Route::post('/users/{user}/assign-role', [\App\Http\Controllers\API\UserController::class,'assignRole'])->name('users.assign-role');
    Route::get('/users/dt', [\App\Http\Controllers\API\UserController::class,'dt'])->name('users.dt');
    Route::apiResource('/users', \App\Http\Controllers\API\UserController::class)->parameters(["users" => "user"]);

    
    Route::get('/permissions/dt', [\App\Http\Controllers\API\PermissionController::class,'dt'])->name('permissions.dt');
    Route::apiResource('/permissions', \App\Http\Controllers\API\PermissionController::class)->parameters(["permissions" => "permission"]);

    
    Route::post('/roles/{role}/assign-permission', [\App\Http\Controllers\API\RoleController::class,'assignPermission'])->name('roles.assign-permission');
    Route::get('/roles/dt', [\App\Http\Controllers\API\RoleController::class,'dt'])->name('roles.dt');
    Route::apiResource('/roles', \App\Http\Controllers\API\RoleController::class)->parameters(["roles" => "role"]);


    Route::get('/modules/dt', [\App\Http\Controllers\API\ModuleController::class,'dt'])->name('modules.dt');
    Route::apiResource('/modules', \App\Http\Controllers\API\ModuleController::class)->parameters(["modules" => "module"]);


    Route::get('/groups/dt', [\App\Http\Controllers\API\GroupController::class,'dt'])->name('groups.dt');
    Route::apiResource('/groups', \App\Http\Controllers\API\GroupController::class)->parameters(["groups" => "group"]);

    
    Route::get('/students/dt', [\App\Http\Controllers\API\StudentController::class,'dt'])->name('students.dt');
    Route::apiResource('/students', \App\Http\Controllers\API\StudentController::class)->parameters(["students" => "student"]);

    
    Route::get('/events/dt', [\App\Http\Controllers\API\EventController::class,'dt'])->name('events.dt');
    Route::apiResource('/events', \App\Http\Controllers\API\EventController::class)->parameters(["events" => "event"]);

    
    Route::get('/session-types/dt', [\App\Http\Controllers\API\SessionTypeController::class,'dt'])->name('session-types.dt');
    Route::apiResource('/session-types', \App\Http\Controllers\API\SessionTypeController::class)->parameters(["session-types" => "sessionType"]);

    
    Route::get('/sessions/dt', [\App\Http\Controllers\API\SessionController::class,'dt'])->name('sessions.dt');
    Route::apiResource('/sessions', \App\Http\Controllers\API\SessionController::class)->parameters(["sessions" => "session"]);

    
    Route::get('/absents/dt', [\App\Http\Controllers\API\AbsentController::class,'dt'])->name('absents.dt');
    Route::apiResource('/absents', \App\Http\Controllers\API\AbsentController::class)->parameters(["absents" => "absent"]);

    
    // Route::get('/agendas/dt', [\App\Http\Controllers\API\AgendaController::class,'dt'])->name('agendas.dt');
    Route::apiResource('/agendas', \App\Http\Controllers\API\AgendaController::class)->parameters(["agendas" => "agenda"]);
    Route::get('/agendas_print', [\App\Http\Controllers\API\AgendaController::class,'print'])->name('agendas_print');

    Route::get('/sales/dt', [\App\Http\Controllers\API\SaleController::class,'dt'])->name('sales.dt');
    Route::apiResource('/sales', \App\Http\Controllers\API\SaleController::class)->parameters(["sales" => "sale"]);

});