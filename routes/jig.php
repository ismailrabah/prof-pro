<?php
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::middleware(['auth:sanctum', 'verified'])->get('/admin', function () {
    return Inertia::render('AdminDashboard');
})->name('admin.dashboard');


/* Auto-generated users admin routes */
Route::group(["prefix" => "admin","as" => "admin.","middleware"=>['auth:sanctum', 'verified']], function () {
    Route::resource('users', \App\Http\Controllers\Admin\UserController::class)->parameters(["users" => "user"]);
    Route::resource('permissions', \App\Http\Controllers\Admin\PermissionController::class)->parameters(["permissions" => "permission"]);
    Route::resource('roles', \App\Http\Controllers\Admin\RoleController::class)->parameters(["roles" => "role"]);
    Route::resource('modules', \App\Http\Controllers\Admin\ModuleController::class)->parameters(["modules" => "module"]);
    Route::resource('groups', \App\Http\Controllers\Admin\GroupController::class)->parameters(["groups" => "group"]);
    Route::resource('students', \App\Http\Controllers\Admin\StudentController::class)->parameters(["students" => "student"]);
    Route::resource('events', \App\Http\Controllers\Admin\EventController::class)->parameters(["events" => "event"]);
    Route::resource('session-types', \App\Http\Controllers\Admin\SessionTypeController::class)->parameters(["session-types" => "sessionType"]);
    Route::resource('sessions', \App\Http\Controllers\Admin\SessionController::class)->parameters(["sessions" => "session"]);
    Route::resource('absents', \App\Http\Controllers\Admin\AbsentController::class)->parameters(["absents" => "absent"]);
    Route::resource('agendas', \App\Http\Controllers\Admin\AgendaController::class)->parameters(["agendas" => "agenda"]);
    Route::resource('sales', \App\Http\Controllers\Admin\SaleController::class)->parameters(["sales" => "sale"]);
});
